<?php
use Phalcon\Http\Request;

// use form
use App\Forms\CreateForm;
use App\Forms\LoginForm;

class UserController extends ControllerBase
{
    public $loginForm;
    public $usersModel;
    public $createForm;
    public function onConstruct()
    {
    }

    public function initialize()
    {
        $this->loginForm = new LoginForm();
        $this->usersModel = new Users();
        $this->createForm = new CreateForm();
    }

    public function loginAction()
    {
        $this->tag->setTitle('Phalcon :: Login');
        $this->view->form = new LoginForm();
    }

    public function loginSubmitAction()
    {
        // check request
        if (!$this->request->isPost()) {
            return $this->response->redirect('user/login');
        }


        // Validate CSRF token
        if (!$this->security->checkToken()) {
            $this->flashSession->error("Invalid Token");
            return $this->response->redirect('user/login');
        }

        $this->loginForm->bind($_POST, $this->usersModel);
        // check form validation
        if (!$this->loginForm->isValid()) {
            foreach ($this->loginForm->getMessages() as $message) {
                $this->flashSession->error($message);
                $this->dispatcher->forward([
                    'controller' => $this->router->getControllerName(),
                    'action'     => 'login',
                ]);
                return;
            }
        }
        
        // login with database
        $email    = $this->request->getPost('email');
        $password = $this->request->getPost('password');

        /**
         * Users::findFirst();
         * $this->usersModel->findFirst();
         */
        $user = Users::findFirst([ 
            'email = :email:',
            'bind' => [
               'email' => $email,
            ]
        ]);
            
        // Check User Active
        if ($user->active != 1) {
            $this->flashSession->error("User Deactivate");
            return $this->response->redirect('user/login');
        }
        
        # Doc :: https://docs.phalconphp.com/en/3.3/security
        if ($user) {
            if ($this->security->checkHash($password, $user->password))
            {
                # https://docs.phalconphp.com/en/3.3/session#start

                // Set a session
                $this->session->set('AUTH_ID', $user->id);
                $this->session->set('AUTH_NAME', $user->name);
                $this->session->set('AUTH_EMAIL', $user->email);
                $this->session->set('AUTH_CREATED', $user->created);
                $this->session->set('AUTH_UPDATED', $user->updated);
                $this->session->set('IS_LOGIN', 1);

                // $this->flashSession->success("Login Success");
                return $this->response->redirect('user/profile');
            }
        } else {
            // To protect against timing attacks. Regardless of whether a user
            // exists or not, the script will take roughly the same amount as
            // it will always be computing a hash.
            $this->security->hash(rand());
        }

        // The validation has failed
        $this->flashSession->error("Invalid login");
        return $this->response->redirect('user/login');
    }


    public function storeAction(){
        $this->tag->setTitle('Phalcon :: Register');
        $this->view->form = new CreateForm();

        // $this->usersModel->setName('Dang Quoc Ki');
        // $this->usersModel->setEmail('kidq@nhanlucsieuviet.com');
        // $this->usersModel->setPassword($this->security->hash('password'));
        // $this->usersModel->setActive(1);
        // $this->usersModel->setCreated(time());
        // $this->usersModel->setUpdated(time());
        // $this->usersModel->save();
    }

    public function profileAction()
    {
        $this->view->users = $this->usersModel->find();
        $this->authorized();
    }

    public function logoutAction()
    {
        # https://docs.phalconphp.com/en/3.3/session#remove-destroy

        // Destroy the whole session
        $this->session->destroy();
        return $this->response->redirect('user/login');
    }

    public function storeSubmitAction()
    {
        $form = new CreateForm(); 
      

        // check request
        if (!$this->request->isPost()) {
            return $this->response->redirect('user/create');
        }

        $form->bind($_POST, $this->usersModel);
        // check form validation
        if (!$form->isValid()) {
            foreach ($form->getMessages() as $message) {
                $this->flashSession->error($message);
                $this->dispatcher->forward([
                    'controller' => $this->router->getControllerName(),
                    'action'     => 'store',
                ]);
                return;
            }
        }

        $this->usersModel->setPassword($this->security->hash($_POST['password']));
        $this->usersModel->setActive(1);
        $this->usersModel->setCreated(time());
        $this->usersModel->setUpdated(time());
        
     
        if (!$this->usersModel->save()) {
            foreach ($this->usersModel->getMessages() as $m) {
                $this->flashSession->error($m);
                $this->dispatcher->forward([
                    'controller' => $this->router->getControllerName(),
                    'action'     => 'store',
                ]);
                return;
            }
        }

        $this->flashSession->success('Create User successfully!');
        return $this->response->redirect('user/profile');

        $this->view->disable();
    }

    public function updateAction($userId = null)
    {
        $url_id = urldecode(strtr($userId,"'",'%'));
        $userId = $this->crypt->decryptBase64($url_id);
        

         // Check article id not empty
         if (!empty($userId) AND $userId != null)
         {
             // Check Post Request
             if($this->request->isPost())
             {
                 # bind user type data
                 $this->createForm->bind($this->request->getPost(), $this->articleModel);
                 $this->view->form = new CreateForm($this->usersModel, [
                     "edit" => true
                 ]);
                 
             } else
             { 
                 $user = Users::findFirst($userId);
                 

                 if (!$user) {
                     $this->flashSession->error('User was not found');
                     return $this->response->redirect('user/profile');
                 }
 
                 // Send Article Data in Article Form
                 $this->view->form = new CreateForm($user, [
                     "edit" => true
                 ]);
             }
         } else {
             return $this->response->redirect('user/profile');
         }
    }

    public function updateSubmitAction()
    {
        // check post request
        if (!$this->request->isPost()) {
            return $this->response->redirect('user/profile');
        }


        // get article id
        $userEid = $this->request->getPost("eid");

        /**
         * Decode Article Eid
         */
        $userId = $this->crypt->decryptBase64(urldecode(strtr($userEid,"'",'%')));

        // Check Agin User Article is Valid
        $user = Users::findFirst($userId);

        if (!$user) {
            $this->flashSession->error('User was not found');
            return $this->response->redirect('user/profile');
        }

        $user->save(
            [
                'name' => $this->request->getPost('name')
            ]
        );

        // Clear Article Form
        $this->createForm->clear();

        $this->flashSession->success('Users was updated successfully.');
        return $this->response->redirect('user/profile');

        $this->view->disable();
    }

    public function deleteAction($userEid)
    {
        $userId = $this->crypt->decryptBase64(urldecode(strtr($userEid,"'",'%')));

        $user = Users::findFirst($userId);

        if (!$user) {
            $this->flashSession->error('User was not found');
            return $this->response->redirect('user/profile');
        }

        $user->save(
            [
                'active' => 0
            ]
        );

        $this->flashSession->success('Users was disable successfully.');
        return $this->response->redirect('user/profile');
    }
}