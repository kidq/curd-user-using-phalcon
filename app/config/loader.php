<?php

$loader = new \Phalcon\Loader();

$loader->registerDirs(
    [
        $config->application->controllersDir,
        $config->application->modelsDir,
    ]
);



$loader->registerNamespaces(
    [
       'App\Forms'  => APP_PATH .'/forms/',
    ]
);

$loader->register();