<?php

$router = $di->getRouter();

// Define your routes here

$router->add('/user/login', ['controller' => 'user', 'action' => 'login']);
$router->add('/user/login/submit', ['controller' => 'user', 'action' => 'loginSubmit']);
$router->add('/user/create', ['controller' => 'user', 'action' => 'store']);
$router->add('/user/create/submit', ['controller' => 'user', 'action' => 'storeSubmit']);
$router->add('/user/update', ['controller' => 'user', 'action' => 'update']);
$router->add('/user/update/submit', ['controller' => 'user', 'action' => 'updateSubmit']);
$router->add('/user/profile', ['controller' => 'user', 'action' => 'profile']);
$router->add('/user/delete', ['controller' => 'user', 'action' => 'delete']);

$router->handle();
