<?php

use Phalcon\Validation;
use Phalcon\Validation\Validator\Email as EmailValidator;
use Phalcon\Validation\Validator\Uniqueness as Uniqueness;

class Users extends \Phalcon\Mvc\Model
{
    protected $id;
    protected $name;
    protected $email;
    protected $password;
    protected $active;
    protected $created;
    protected $updated;

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

   
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

   
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }


    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }


    public function getName()
    {
        return $this->name;
    }


    public function getEmail()
    {
        return $this->email;
    }


    public function getPassword()
    {
        return $this->password;
    }

    public function getActive()
    {
        return $this->active;
    }

    public function getCreated()
    {
        return $this->created;
    }

    public function getUpdated()
    {
        return $this->updated;
    }


    public function validation()
    {
        $validator = new Validation();

        $validator->add(
            'email',
            new EmailValidator(
                [
                    'model'   => $this,
                    'message' => 'Please enter a correct email address',
                ]
            )
        );

        $validator->add(
            'email',
            new Uniqueness(
                [
                    'model'   => $this,
                    'message' => 'Another user with same email already exists',
                    'cancelOnFail' => true,
                ]
            )
        );

        return $this->validate($validator);
    }


    public function initialize()
    {
        $this->setSchema("test");
        $this->setSource("users");
    }


    public static function find($parameters = null)
    {
        return parent::find($parameters);
    }

    /**
     * Allows to query the first record that match the specified conditions
     *
     * @param mixed $parameters
     * @return Users|\Phalcon\Mvc\Model\ResultInterface
     */
    public static function findFirst($parameters = null)
    {
        return parent::findFirst($parameters);
    }

    /**
     * Independent Column Mapping.
     * Keys are the real names in the table and the values their names in the application
     *
     * @return array
     */
    public function columnMap()
    {
        return [
            'id' => 'id',
            'name' => 'name',
            'email' => 'email',
            'password' => 'password',
            'active' => 'active',
            'created' => 'created',
            'updated' => 'updated',
        ];
    }

    /**
     * Returns table name mapped in the model.
     *
     * @return string
     */
    public function getSource()
    {
        return 'users';
    }
}
?>